#!/bin/bash

# Juste pour comprendre le code
from="$1"
config="$2"

# add var for debug
www_dir=''; log_dir=''; certs_dir=''; skynet_dir=''; vh_dns=''; vh_name=''; vh_mail=''; vh_alias=''; vh_remove=''; vh_public=''; 

# shellcheck source=/dev/null
. "$config"

httpd='apache2'
sysctl='systemctl restart'
vhost_avbl_dir='/etc/apache2/sites-available'
vhost_enbl_dir='/etc/apache2/sites-enabled'

log='/tmp/auto_addWebSite.log'

err()
{
    echo "FATAL ERROR: ${1}"
    [[ -n $log ]] && echo "[$(date)] FATAL ERROR: ${1}" >> "$log"

    exit 1
}

vhRemove()
{
    [[ -e $vhost_enbl_dir/$vh_dns.conf ]] || return

    rm "$www_dir/$vh_dns"
    rm "$vhost_enbl_dir/$vh_dns.conf"
}

for vhost in "$from"/*; do
    # shellcheck source=/dev/null
    . "$vhost"

    _alias=''
    _docRoot="$www_dir/$vh_dns"
    _certPath="$certs_dir/$vh_dns"

    vhRemove

    [[ $vh_remove == "true" ]] && continue
    [[ -n $vh_alias ]] && _alias="ServerAlias $vh_alias"

    [[ -f $_certPath/chain.pem ]] || err "no certificate found for: $vh_dns"
    ln -s "$skynet_dir/$vh_name/$vh_public" "$_docRoot" || err "can't create symlink to: $_docRoot"

    echo "
        <VirtualHost *:443>
            ServerName $vh_dns
            $_alias
            ServerAdmin $vh_mail

            DocumentRoot $_docRoot/
            <Directory $_docRoot/>
              Options -Indexes +FollowSymlinks
              Require all granted
              AllowOverride All
            </Directory>

            SSLEngine on
            SSLCertificateFile $_certPath/fullchain.pem
            SSLCertificateKeyFile $_certPath/privkey.pem
            SSLCertificateChainFile $_certPath/chain.pem

            SSLCACertificatePath $_certPath/
            SSLCACertificateFile $_certPath/chain.pem

            ErrorLog $log_dir/$vh_dns.error.log
            CustomLog $log_dir/$vh_dns.access.log combined
        </VirtualHost>
        " > "$vhost_avbl_dir/$vh_dns.conf"

    ln -s "$vhost_avbl_dir/$vh_dns.conf" "$vhost_enbl_dir/$vh_dns.conf" || err "can't create symlink to: $vhost_enbl_dir/$vh_dns.conf"
done

$sysctl $httpd || err "httpd restart error"
